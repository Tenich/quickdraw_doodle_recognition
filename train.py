#!/usr/bin/env python
# coding: utf-8

import argparse
import os

import pandas as pd
import numpy as np

import torch
from torch import nn, optim
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader

from torchvision import transforms

from albumentations import Compose, Rotate, Resize

import pretrainedmodels
import pretrainedmodels.utils as utils

import cv2

from tqdm import tqdm as tqdm_notebook
from average_precision import mapk

import lera


DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu") 

MODEL_NAME = 'se_resnext101_32x4d'
BATCH_SIZE = 32 * torch.cuda.device_count()

TRAIN_DIR = "./data/train_simplified/"
TRAIN_DIR = os.path.expanduser(TRAIN_DIR)
TRAIN_FILES = os.listdir(TRAIN_DIR)

CLASSES = sorted([f[:-4] for f in TRAIN_FILES])
CLASS_TO_IDX = {word: i for i, word in enumerate(sorted(CLASSES))}

TRAIN_PARTS = 5

TRAIN_FILE = './data/train_{}.csv'
VAL_FILE = './data/val.csv'
TEST_FILE = './data/test_simplified.csv'

N_CLASSES = len(CLASSES)


### data methods
def augment():
    return Compose([Rotate(limit=10, border_mode=cv2.BORDER_CONSTANT)])

def drawing_to_np(drawing, output_size=(224, 224), augment=False):
    drawing = eval(drawing)
    if augment:
        np.random.shuffle(drawing)
    
    img = np.zeros((256, 256, 3), dtype=np.uint8)    
    for i, (stroke_x, stroke_y) in enumerate(drawing):
        for (x, y), (x_next, y_next) in zip(zip(stroke_x, stroke_y),
                                            zip(stroke_x[1:], stroke_y[1:])):
            cv2.line(img, (x, y), (x_next, y_next), (255, 0, 0), 5)
            cv2.line(img, (x, y), (x_next, y_next), (0, 255, 0), 3)
            cv2.line(img, (x, y), (x_next, y_next), (0, 0, 255 * (i + 1) / len(drawing)), 3)
    img = cv2.resize(img, output_size)
    return img

class DoodleDataset(Dataset):
    def __init__(self, file_path, augment=None):
        self.df = pd.read_csv(file_path, low_memory=True, compression='zip')
        self.len = len(self.df)
        
        self.augment = augment
        self.preprocess = transforms.Compose([transforms.ToTensor()])
        
    def __len__(self):
        return self.len
    
    def __getitem__(self, idx):
        sample = self.df.iloc[idx]
        
        if self.augment is not None:
            image = drawing_to_np(sample['drawing'], augment=True)
            image = self.augment(image=image)['image']
        else:
            image = drawing_to_np(sample['drawing'])
        image = self.preprocess(image)
        
        if 'word' in sample:
            return {'image': image,
                    'class': sample['word']}
        else:
            return {'image': image}


### metrics methods
def accuracy(preds, classes):
    return (preds.max(dim=1)[1] == classes).float().mean().item()

def score(preds, classes):
    preds = list(preds.topk(3, dim=1)[1].cpu().numpy())
    targets = [[x] for x in classes.cpu().numpy()]
    
    return mapk(targets, preds, 3)


# model methods
def get_pretrained_model():
    model = pretrainedmodels.__dict__[MODEL_NAME](num_classes=1000, pretrained='imagenet')
    model.last_linear = nn.Linear(2048, len(CLASSES))
    
    return model      

class ClassificationNet(nn.Module):
    def __init__(self, criterion=nn.CrossEntropyLoss()):
        super().__init__()
        
        self.model = get_pretrained_model()
        self.criterion = criterion
        
    def forward(self, x, y=None):
        output = self.model(x)
        
        if y is not None:
            return output, self.criterion(output, y)
        else:
            return output
        
def get_model():
    model = ClassificationNet()
    
    if torch.cuda.device_count() > 1:
        model = nn.DataParallel(model)

    model.to(DEVICE)
        
    return model


## training methods
def train(model, optimizer, train_loader, model_save_path, max_epochs=1, max_steps=None): 
    cnt = 0
    
    for epoch in tqdm_notebook(range(max_epochs)):
        running_score = 0.
        exp_running_score = 0.
        model.train()
        
        for batch in tqdm_notebook(train_loader):
            cnt += 1
            
            images = batch['image'].to(DEVICE)
            classes = batch['class'].to(DEVICE)

            optimizer.zero_grad()

            preds, loss = model(images, classes)
            loss = loss.mean()

            loss.backward()
            optimizer.step()

            acc, sc = accuracy(preds, classes), score(preds, classes)
            running_score += sc
            exp_running_score = (1 - 1e-2) * exp_running_score + 1e-2 * sc

            lera.log('ce', loss.item())
            lera.log('acc', acc)
            lera.log('score', sc)
            lera.log('running_score', running_score / cnt)
            lera.log('exp_running_score', exp_running_score)

            if cnt % 10000 == 0:
                torch.save(model.state_dict(), model_save_path)

            if max_steps is not None and cnt >= max_steps:
                break


def load_weights(model, weights_path):
    state_dict = torch.load(weights_path, 
                            map_location=lambda x, y: x)

    try:
        try:
            model.load_state_dict(state_dict)
        except:
            state_dict = {k[7:]: v for k, v in state_dict.items()}
            model.load_state_dict(state_dict)
        model.to(DEVICE)
        print("pretrained weights loaded")
    except:
        print("pretrained weights failed")
    
                
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--idx", type=int, required=True)
    parser.add_argument("--continue", dest='cnt', action='store_true')
    arguments = parser.parse_args()
    
    
    MODEL_SAVE_PATH = './data/{}_{}.pth'.format(MODEL_NAME, arguments.idx)
    lera.log_hyperparams({
        'title': 'QuickDraw {} on fold {}'.format(MODEL_NAME, arguments.idx),
        'batch_size': BATCH_SIZE,
        'optimizer': 'Adam with amsgrad'
    }) 

    print(MODEL_SAVE_PATH)
    
    train_dataset = DoodleDataset(TRAIN_FILE.format(arguments.idx), augment=augment())
    train_loader = DataLoader(train_dataset, batch_size=BATCH_SIZE, shuffle=True, num_workers=2)
    n_batches = len(train_loader)

    model = get_model()
   
    if not arguments.cnt: 
        # pretrain last layer
        try:
            optimizer = optim.Adam(model.model.last_linear.parameters(), lr=1e-3, amsgrad=True)
        except:
     	    optimizer = optim.Adam(model.module.model.last_linear.parameters(), lr=1e-3, amsgrad=True)
        train(model, optimizer, train_loader, MODEL_SAVE_PATH, max_steps=1000)
    
        # train more on full data
        optimizer = optim.Adam(model.parameters(), lr=1e-4, amsgrad=True)
    
        train(model, optimizer, train_loader, MODEL_SAVE_PATH)
    
        optimizer.param_groups[0]['lr'] *= 0.1
        train(model, optimizer, train_loader, MODEL_SAVE_PATH)
    else:
         load_weights(model, MODEL_SAVE_PATH)
         optimizer = optim.Adam(model.parameters(), lr=5e-6, amsgrad=True)
         train(model, optimizer, train_loader, MODEL_SAVE_PATH)
   
 
    torch.save(model.state_dict(), MODEL_SAVE_PATH)
    
    
if __name__ == '__main__':
    main()
